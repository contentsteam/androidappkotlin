package br.ola.android.olaandroid

import android.app.Activity
import android.os.Bundle
import android.widget.ListView
import android.widget.Toast
import android.content.Intent



class ListaCustomizada : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_customizada)

        val listaContatos = ArrayList<Contato>()
        val c1 = Contato(1L, "Collor", "color@br.com", "Alagoas")
        val c2 = Contato(20L, "Dilma", "dilma@br.com", "Porto Alegre")

        listaContatos.add(c1)
        listaContatos.add(c2)

        val adapter = ContatoAdapter(applicationContext, listaContatos, getAssets())

        val lista = findViewById<ListView>(R.id.lista)
        lista.setAdapter(adapter)
        lista.setOnItemClickListener {parent, view, position, id ->
            val contato = listaContatos.get(position)
            val intent = Intent(this, DetalheContato::class.java)
            intent.putExtra("contato", contato)
            startActivity(intent)
        }
    }
}
