package br.ola.android.olaandroid

import android.app.Activity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_hello_android.*
import java.util.logging.Logger

class HelloAndroid : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)

        Logger.getLogger(HelloAndroid::class.java.name).info("Ola Log Cat")
        Log.i("aula" , "Ola logcat ")

    }
}
