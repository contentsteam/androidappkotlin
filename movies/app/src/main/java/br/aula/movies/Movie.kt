package br.aula.movies

class Movie(val Title: String = "",
            val Year: String = "",
            val Type: String = "",
            val Poster: String = "")