package br.ola.android.olaandroid

import android.app.Activity
import android.os.Bundle
import android.widget.TextView

class BoasVindas : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_boas_vindas)

        val intent = intent
        if (intent != null) {
            val login = intent.getStringExtra("login")
            val senha = intent.getStringExtra("senha")

            val campoLogin = findViewById<TextView>(R.id.usuario)
            campoLogin.text = login

            val campoSenha = findViewById<TextView>(R.id.senha)
            campoSenha.text = senha
        }
    }
}
