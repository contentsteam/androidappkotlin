package br.aula.movies

import android.os.Bundle
import android.widget.ArrayAdapter

import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import br.aula.movies.Constants.API_KEY
import br.aula.movies.Constants.URL_SERVIDOR

import org.jetbrains.anko.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        getMovies()
    }


    private fun getMovies() {
        var displayMessage = indeterminateProgressDialog("Aguarde")
        displayMessage.show()

        doAsync{
            val movies = Util.parse("$URL_SERVIDOR/?apikey=$API_KEY&type=movie&r=json&s=brazil&page=1") as List<String>

            uiThread{
                movies.let {
                    val adapter = ArrayAdapter(applicationContext, android.R.layout.simple_list_item_1, movies)
                    var lista = findViewById(R.id.movies) as ListView
                    lista.adapter = adapter
                }
            }
            displayMessage.dismiss()
        }
    }


}

