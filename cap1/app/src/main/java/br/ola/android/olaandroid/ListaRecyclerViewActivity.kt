package br.ola.android.olaandroid

import android.app.Activity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.StaggeredGridLayoutManager
import kotlinx.android.synthetic.main.activity_lista_recycler_view.*

class ListaRecyclerViewActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_recycler_view)

        val listaContatos = ArrayList<Contato>()
        val c1 = Contato(1L, "Collor", "color@br.com", "Alagoas")
        val c2 = Contato(20L, "Dilma", "dilma@br.com", "Porto Alegre")

        listaContatos.add(c1)
        listaContatos.add(c2)

        val recyclerView = list_recyclerview
        recyclerView.adapter = ListaRecyclerAdapter(listaContatos, this, getAssets())
        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager

    }
}
