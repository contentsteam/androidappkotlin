package br.aula.movies

data class MovieData(val totalResults: String = "",
                     val Response: String = "True",
                     val Search: List<Movie>? = null )