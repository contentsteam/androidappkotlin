package br.ola.android.olaandroid

import android.app.Activity
import android.widget.Toast
import android.widget.EditText
import android.os.Bundle
import android.widget.Button
import android.content.Intent
import android.net.Uri

class TelaLogin : Activity(){

    internal lateinit var campoLogin: EditText
    internal lateinit var campoSenha: EditText

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)

        this.campoLogin = findViewById<EditText>(R.id.campoLogin)
        this.campoSenha = findViewById<EditText>(R.id.campoSenha)

        val botaoLogin = findViewById<Button>(R.id.login)
        botaoLogin.setOnClickListener {
            val intent = Intent(this@TelaLogin, BoasVindas::class.java)
            intent.putExtra("login", campoLogin.text)
            intent.putExtra("senha", campoSenha.text)
            startActivity(intent)
        }

        val botaoCancel = findViewById<Button>(R.id.cancelar)
        botaoCancel?.setOnClickListener {
            val uri = Uri.parse("tel:999999999")
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
    }
}