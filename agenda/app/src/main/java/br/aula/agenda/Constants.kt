package br.aula.agenda

import java.text.SimpleDateFormat

const val CONTATOS_DB_NAME = "agenda.db"
const val CONTATOS_TABLE_NAME = "contatos"
val dateFormatter = SimpleDateFormat("dd/MM/yyyy")