package br.aula.movies

import com.beust.klaxon.Klaxon
import java.net.URL

object Util {

    fun parse(path: String?): List<String>?{
        val contents = URL(path).readText()
        val movieData = contents?.let { Klaxon().parse<MovieData>(it) }
        return movieData?.Search?.map{it.Title}
    }
}
